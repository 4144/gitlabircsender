#!/bin/bash

python-coverage erase
python-coverage run --source gitlabircsender.py ./test.py
python-coverage annotate gitlabircsender.py
