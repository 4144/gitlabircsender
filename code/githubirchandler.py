#! /usr/bin/env python
# -*- coding: utf8 -*-
#
#  gitlab irc sender
#  Copyright (C) 2016-2017  Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

from code.configuration import Configuration
from code.core import Core

class GithubIrcHandler():
    def printBuildFailed(self, channel, project, branch, data, commit, url, description):
        Core.messageHandler.add(channel, Configuration.irc_github_build_failed_message.format(
            project = project,
            shortid = commit[:Configuration.commit_size],
            branch = branch,
            url = url,
            description = description
        ))

    def printBuildSuccess(self, channel, project, branch, data, commit, url, description):
        Core.messageHandler.add(channel, Configuration.irc_github_build_success_message.format(
            project = project,
            shortid = commit[:Configuration.commit_size],
            branch = branch,
            url = url,
            description = description
        ))

    def printCustomMessage(self, channel, message):
        Core.messageHandler.add(channel, message)

    def printIssuesOpened(self, channel, project, data, url, iid, title):
        Core.messageHandler.add(channel, Configuration.irc_github_issues_opened_message.format(
            project = project,
            url = url,
            author = data["sender"]["login"],
            title = title,
            id = iid
        ))

    def printIssuesEdited(self, channel, project, data, url, iid, title):
        Core.messageHandler.add(channel, Configuration.irc_github_issues_edited_message.format(
            project = project,
            url = url,
            author = data["sender"]["login"],
            title = title,
            id = iid
        ))

    def printIssuesClosed(self, channel, project, data, url, iid, title):
        Core.messageHandler.add(channel, Configuration.irc_github_issues_closed_message.format(
            project = project,
            url = url,
            author = data["sender"]["login"],
            title = title,
            id = iid
        ))

    def printIssuesReopened(self, channel, project, data, url, iid, title):
        Core.messageHandler.add(channel, Configuration.irc_github_issues_reopened_message.format(
            project = project,
            url = url,
            author = data["sender"]["login"],
            title = title,
            id = iid
        ))

    def printIssueCommentCreated(self, channel, project, data, url, iid, title):
        Core.messageHandler.add(channel, Configuration.irc_github_issue_comment_created_message.format(
            project = project,
            url = url,
            author = data["sender"]["login"],
            title = title,
            id = iid
        ))

    def printIssueCommentEdited(self, channel, project, data, url, iid, title):
        Core.messageHandler.add(channel, Configuration.irc_github_issue_comment_edited_message.format(
            project = project,
            url = url,
            author = data["sender"]["login"],
            title = title,
            id = iid
        ))

    def printIssueCommentDeleted(self, channel, project, data, url, iid, title):
        Core.messageHandler.add(channel, Configuration.irc_github_issue_comment_deleted_message.format(
            project = project,
            url = url,
            author = data["sender"]["login"],
            title = title,
            id = iid
        ))
