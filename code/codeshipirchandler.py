#! /usr/bin/env python
# -*- coding: utf8 -*-
#
#  gitlab irc sender
#  Copyright (C) 2016-2017  Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

from code.configuration import Configuration
from code.core import Core

class CodeshipIrcHandler():
    def printBuildFailed(self, channel, project, branch, data, commit_id, commit_url, url, build_id):
        Core.messageHandler.add(channel, Configuration.irc_codeship_build_failed_message.format(
            project = project,
            shortid = commit_id[:Configuration.commit_size],
            branch = branch,
            url = url,
            commit_url = commit_url,
            build = build_id
        ))


    def printBuildSuccess(self, channel, project, branch, data, commit_id, commit_url, url, build_id):
        Core.messageHandler.add(channel, Configuration.irc_codeship_build_success_message.format(
            project = project,
            shortid = commit_id[:Configuration.commit_size],
            branch = branch,
            url = url,
            commit_url = commit_url,
            build = build_id
        ))


    def printCustomMessage(self, channel, message):
        Core.messageHandler.add(channel, message)

