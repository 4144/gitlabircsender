#! /usr/bin/env python
# -*- coding: utf8 -*-
#
#  gitlab irc sender
#  Copyright (C) 2016-2017  Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys

from code.core import Core

class Logger:
    @staticmethod
    def addLog(data):
        Core.log.info(data)
        if Core.config.show_info == True:
            sys.stderr.write(data + "\n")

    @staticmethod
    def addError(data):
        Core.log.error(data)
        if Core.config.show_info == True:
            sys.stderr.write(data + "\n")

