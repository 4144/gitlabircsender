Gitlab irc sender
========

[![build status](https://gitlab.com/4144/gitlabircsender/badges/master/build.svg)](https://gitlab.com/4144/gitlabircsender/commits/master)

What is this?
---------

This tool allow accept web hooks from services like gitlab and depend on settings resend it to irc.
It support for now all known web hook types in gitlab.

Supported web hook senders:

- gitlab
- github (only status web hook)
- codeship

What irc bots supported?
---------

It support irc bot [ii](http://tools.suckless.org/ii/) only.
Why [ii](http://tools.suckless.org/ii/)? Because it without overhead and allow send messages to irc from any language or shell

How gitlab irc sender works?
---------

It start own web server on selected address and port and accept HTTP requests from web hooks.
Accept one of configured hook type.
Generate irc message and send to worker thread.
Worker thread with configured period send messages to one of irc channels.

What language used?
---------

Gitlab irc sender written on python.
Can works on python 2 and python 3

How to use?
---------

- Install [ii](http://tools.suckless.org/ii/) and python and python yaml and python requests
  - debian/ubuntu example:

~~~
apt-get install python ii python-yaml python-requests
~~~

- run [ii](http://tools.suckless.org/ii/) for example like this:

~~~
  ii -s irc.freenode.org -i bot/irc.freenode.net -n mycoolbot -f mycoolbot
~~~

- join in bot into channels what you need
- add this channels into config.yaml
- add/change port in config.yaml
- start gitlab irc sender:

~~~
./gitlabircsender.py
~~~

- now you can create irc hooks and point it to your bot
- press "test hook" button in gitlab, it will send some last commits.

Example configuration
---------

[config.yaml.example](config.yaml.example)

For use copy it to config.yaml

Known limitations
---------

Supported only one irc server at one instance.
